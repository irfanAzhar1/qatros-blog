# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
require 'faker'

100.times do |id|
  User.create(
    name: Faker::Name.name,
    email: Faker::Internet.email,
    password: "qwerty123",
    bio: Faker::Lorem.paragraph,
    dob: Faker::Date.birthday(min_age: 18, max_age: 65),
    city: Faker::Address.city,
    email_confirmed: true,
    confirm_token: nil
  )
end

User.all.each do | user |
  5.times do
    user.posts.create(
      title: Faker::Lorem.sentence,
      content: Faker::Lorem.paragraph
    )
  end
end

Post.all.each do | post |
  5.times do
    post.comments.create(
      body: Faker::Lorem.paragraph,
      user_id: User.all.sample.id
    )
  end
end
