Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  namespace :api do
    namespace :v1 do
      post 'login', to: 'authentication#login'
      options 'login', to: 'authentication#login'
      resources 'users' do
        get 'confirmation/:token', to: 'users#confirm_email'
      end
      resources 'posts' do
        resources 'comments'
      end
    end
  end
end
