Feature: Update User Profile
  as user you want to update your profile

  Background:
    i have login with my account with id "1"

  Scenario: i change my bio, dob, and city
    Given i change my "bio" to "mahasiswa biasa"
    And i change my "dob" to "2000-09-18"
    And i change my "city" to "payakumbuh"
    When i update my account
    Then my "account" should not be updated
