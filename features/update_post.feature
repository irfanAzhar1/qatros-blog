Feature: Update a post
  as a user i want to update my post

  Background:
    i have login with my account with id "1"
    i open my first post

  Scenario: Update a post
    Given i change my post "title" to "new title"
    And i change my post "content" to "new content"
    When i update my post
    Then my "post" should be updated
