Feature: Create Post
  as a user i want to add new post

  Background:
    i have login with my account with id "1"


  Scenario: Create a post with valid data
    Given i am on "create new" post form
    And i fill in "title" with "post 1"
    And i fill in "content" with "deskripsi post 1"
    When i submit my post
    Then my "post" should be created

  Scenario: Create a post with title blank
    Given i am on "create new" post form
    And i fill in "content" with "deskripsi post 1"
    When i submit my post
    Then my "post" should not be created

  Scenario: Create a post with content blank
    Given i am on "create new" post form
    And i fill in "title" with "post 1"
    When i submit my post
    Then my "post" should not be created

  Scenario: Create a post with title and content blank
    Given i am on "create new" post form
    When i submit my post
    Then my "post" should not be created

  Scenario: Create a post without user_id
    Given i am on "create new" post form
    And i fill in "title" with "post 1"
    And i fill in "content" with "deskripsi post 1"
    And i am logout from my account
    When i submit my post
    Then my "post" should not be created