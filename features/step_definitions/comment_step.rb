Before do
  @user = User.find(1)
  @post = Post.find(1)
end

Given("i am on the comment text field") do
  @params = {
    body: ""
  }
end

Given("i open my first comment") do
  @comment = @user.comments.first
end

Given("i change my comment {string} to {string}") do |attribute, value|
  @comment[attribute] = value
end

When("i submit my comment") do
  @comment = Comment.new(@params)
  @comment.user = @user
  @comment.post = @post
  @response = @comment.save
end