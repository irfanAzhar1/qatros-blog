Given("i am on the register endpoint") do
  @params = {
    "name": "",
    "email": "",
    "password": ""
  }
end

Given("i have many posts") do
  @posts = @user.posts
end

Given("i have many comments") do
  @comments = @user.comments
end

When("i press register") do
  @user = User.new(@params)
  @response = @user.save
end

When("i register again with same email") do
  @user = User.new(@params)
  @response = @user.save
end

When("i change my {string} to {string}") do |attribute, value|
  @user[attribute] = value
end

When("i update my account") do
  @response = @user.update({
    name: @user.name,
    email: @user.email,
    password: @user.password,
    bio: @user.bio,
    dob: @user.dob,
    city: @user.city
  })
end

When("i delete my account") do
  @response = @user.destroy
end

Then("all my posts should be deleted") do
  @posts.each do |post|
    expect(Post.find_by(id: post.id)).to eq(nil)
  end
end

Then("all my comments should be deleted") do
  @comments.each do |comment|
    expect(Comment.find_by(id: comment.id)).to eq(nil)
  end
end
