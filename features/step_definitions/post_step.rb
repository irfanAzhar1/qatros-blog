Before do
  @user = User.find(1)
end

Given("i am on {string} post form") do |action|
  @params = {
    title: "",
    content: ""
  }
end

Given("i am logout from my account") do
  @user = nil
end

Given("i open my first post") do
  @post = @user.posts.first
end

Given("i change my post {string} to {string}") do |attribute, value|
  @post[attribute] = value
end

Given("my post has many comments") do
  @comments = @post.comments
end

When("i update my post") do
  @response = @post.update({
    title: @post.title,
    content: @post.content
  })
end

When("i submit my post") do
  @post = Post.new(@params)
  @post.user = @user
  @response = @post.save
end

When("i delete my post") do
  @response = @post.destroy
end

Then("my post's comments should be deleted") do
  @comments.each do |comment|
    expect(Comment.find_by_id(comment.id)).to eq(nil)
  end
end