Given('i fill in {string} with {string}') do |attribute, value|
  @params[attribute] = value
end

Then("my {string} should be created") do |model|
  expect(@response).to eq(true)
end

Then("my {string} should not be created") do |model|
  expect(@response).to eq(false)
end

Then("my {string} should be updated") do |model|
  expect(@response).to eq(true)
end

Then("my {string} should not be updated") do |model|
  expect(@response).to eq(false)
end
