Feature: Delete user
  as a user i want to delete my account

  Background:
  i have login with my account with id "99"

  Scenario: Delete user
    Given i have many posts
    And i have many comments
    When i delete my account
    And all my posts should be deleted
    And all my comments should be deleted