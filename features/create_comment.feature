Feature: Create Comment
  i see an interesting post
  and i want comment on it

  Background:
    i have login with my account with id "1"
    i see post with id "1"

  Scenario: Create Comment with body
    Given i am on the comment text field
    And i fill in "body" with "this is a comment"
    When i submit my comment
    Then my "comment" should be created

  Scenario: Create Comment without body
    Given i am on the comment text field
    When i submit my comment
    Then my "comment" should not be created