Feature: Delete Post
  as a user i want to delete my post

  Background:
    i have login with my account with id "2"

  Scenario:
    Given i open my first post
    And my post has many comments
    When i delete my post
    And my post's comments should be deleted