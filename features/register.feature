Feature: Register
  i want to use blog app
  so i must register an account

  Scenario: Register with valid data
    Given i am on the register endpoint
    And i fill in "email" with "irfan@mail.com"
    And i fill in "password" with "qwerty123"
    And i fill in "name" with "irfan"
    When i press register
    Then my "account" should be created

  Scenario: Register with name blank
    Given i am on the register endpoint
    And i fill in "email" with "irfan@mail.com"
    And i fill in "password" with "qwerty123"
    When i press register
    Then my "account" should not be created

  Scenario: Register with email blank
    Given i am on the register endpoint
    And i fill in "password" with "qwerty123"
    And i fill in "name" with "irfan"
    When i press register
    Then my "account" should not be created

  Scenario: Register with password blank
    Given i am on the register endpoint
    And i fill in "email" with "irfan@mail.com"
    And i fill in "name" with "irfan"
    When i press register
    Then my "account" should not be created

  Scenario: Register with invalid email
    Given i am on the register endpoint
    And i fill in "email" with "irfan.com"
    And i fill in "password" with "qwerty123"
    And i fill in "name" with "irfan"
    When i press register
    Then my "account" should not be created

  Scenario: Register with email that already exist
    Given i am on the register endpoint
    And i fill in "email" with "irfan@mail.com"
    And i fill in "password" with "qwerty123"
    And i fill in "name" with "irfan"
    And i press register
    When i register again with same email
    Then my "account" should not be created
