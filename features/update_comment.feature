Feature: Update Comment
  as a user i want to update my comment

  Background:
    i have login with my account with id "1"
    i open my first comment

  Scenario:
    Given i change my comment "body" to "new content"
    When i update my comment
    Then my "comment" should be updated