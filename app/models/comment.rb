class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :post

  validates :body, presence: true
  validates :user_id, presence: true
  validates :post_id, presence: true

  def new_attribute
    {
      id: self.id,
      body: self.body,
      user: {
        id: self.user.id,
        name: self.user.name
      },
      post_id: self.post_id
    }
  end
end
