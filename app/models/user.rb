class User < ApplicationRecord
  before_create :confirmation_token
  require "securerandom"
  has_secure_password

  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy

  validates :email, presence: true, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password, presence: true
  validates :name, presence: true

  def new_attribute
    {
      id: self.id,
      name: self.name,
      email: self.email,
      dob: self.dob,
      city: self.city,
      bio: self.bio,
    }
  end

  def email_valid?
    if self.email.match(URI::MailTo::EMAIL_REGEXP)
      return true
    else
      return false
    end
  end

  def email_activate
    self.email_confirmed = true
    self.confirm_token = nil
    save!(:validate => false)
  end

  private
  def confirmation_token
    if self.confirm_token.blank?
      self.confirm_token = SecureRandom.urlsafe_base64.to_s
    end
  end
end
