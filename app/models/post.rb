class Post < ApplicationRecord
  has_many :comments, dependent: :destroy
  belongs_to :user

  validates :title, presence: true
  validates :content, presence: true
  validates :user_id, presence: true

  def new_attribute
    {
      id: self.id,
      title: self.title,
      content: self.content,
      user: {
        id: self.user.id,
        name: self.user.name
      },
      comments: self.comments.map do |comment|
        {
          id: comment.id,
          body: comment.body,
          user: {
            id: comment.user.id,
            name: comment.user.name
          }
        }
      end,
      comment_count: self.comments.count,
    }
  end
end
