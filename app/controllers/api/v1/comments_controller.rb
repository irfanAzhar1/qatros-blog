class Api::V1::CommentsController < ApplicationController
  before_action :set_post

  def index
    limit = params[:limit].to_i
    page = params[:page].to_i
    offset = page * limit
    @comments = @post.comments.order(:updated_at).limit(limit).offset(offset)
    render json: {
      page: page,
      total_items: @post.comments.count,
      total_pages: (@post.comments.count.to_f / limit).ceil,
      data: {
        comments: @comments.map{ |comment| comment.new_attribute}
      }
    }
  end

  def show
    @comment = @post.comments.find(params[:id])
    render json: @comment.new_attribute
  end

  def create
    @comment = @post.comments.new(comment_params)
    if @comment.save
      render json: @comment.new_attribute
    else
      render json: {"message": @comment.errors}, status: :bad_request
    end
  end

  def update
    @comment = @post.comments.find(params[:id])
    if @comment
      @comment.update(comment_params)
      render json: @comment.new_attribute
    else
      render json: {"message": "Comment not found"}
    end
  end

  def destroy
    @comment = @post.comments.find(params[:id])
    if @comment
      @comment.destroy
    else
      render json: {"message": "Comment not found"}
    end
  end

  private

  def set_post
    @post = Post.find(params[:post_id])
  end

  def comment_params
    {
      body: params[:comment],
      user_id: @current_user.id
    }
  end
end
