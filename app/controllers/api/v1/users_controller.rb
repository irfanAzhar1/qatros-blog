class Api::V1::UsersController < ApplicationController
  skip_before_action :authenticate_request, only: [:create, :confirm_email]

  def index
    limit = params[:limit] ? params[:limit].to_i : 5
    search = params[:name] ? params[:name] : ''
    @users = User.where("lower(name) LIKE :search", search: "%#{search}%").order(:updated_at).limit(limit)
    render json: {
      data: {
        users: @users.map{ |user| user.new_attribute}
      }
    }
  end

  def show
    @user = User.find(params[:id])
    render json: @user.new_attribute
  end

  def create
    @user = User.new(user_params)

    return render json: {"message": "Name can't be blank"} if @user.name == nil

    return render json: {"message": "Email can't be blank"} if @user.email == nil

    return render json: {"message": "Password can't be blank"} if @user.password == nil

    return render json: {"message": "Email is invalid"} if !@user.email_valid?

    if @user.save
      UserMailer.with(user: @user).registration_confirmation.deliver_now
      render json: {message: "User created successfully, please confirm your email address to continue"}, status: :created
    else
      render json: {"message": @user.errors}, status: :bad_request
    end
  end

  def confirm_email
    user = User.find_by_confirm_token(params[:token])
    if user
      user.email_activate
      render json: {message: "Welcome to the Sample App! Your email has been confirmed.
      Please sign in to continue."}
    else
      render json: {message: "Sorry. User does not exist"}
    end
  end

  def update
    return render json: {message: "unauthorized"}, status: :unauthorized if @current_user.id != params[:id].to_i
    @user = User.find(params[:id])
    if @user.update(user_params)
      render json: @user.new_attribute
    else
      render json: {"message": "User not found"}
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user
      @user.destroy
    else
      render json: {"message": "User not found"}
    end
  end

  private

  def user_params
    {
      email: params[:email],
      password: params[:password],
      name: params[:name],
      dob: params[:dob],
      city: params[:city],
      bio: params[:bio]
    }
  end
end
