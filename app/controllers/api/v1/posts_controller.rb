class Api::V1::PostsController < ApplicationController

  def index
    limit = params[:limit].to_i
    page = params[:page].to_i
    offset = page * limit
    user_id = params[:user_id].present? ? params[:user_id] : nil
    if user_id.present?
      @posts = Post.where(user_id: user_id).order("updated_at DESC").limit(limit).offset(offset)
      total_items = Post.where(user_id: user_id).count
      total_pages = (total_items.to_f / limit).ceil
    else
      @posts = Post.order("updated_at DESC").limit(limit).offset(offset)
      total_items = Post.count
      total_pages = (total_items.to_f / limit).ceil
    end
    render json: {
      page: page,
      total_items: total_items,
      total_pages: total_pages,
      data: {
        posts: @posts.map{ |post| post.new_attribute}
      }
    }
  end

  def show
    @post = Post.find(params[:id])
    render json: @post.new_attribute
  end

  def create
    @post = Post.new(post_params)
    if @post.save
      render json: @post.new_attribute
    else
      render json: {"message": @post.errors}, status: :bad_request
    end
  end

  def update
    @post = Post.find(params[:id])
    if @post
      @post.update(post_params)
      render json: @post.new_attribute
    else
      render json: {"message": "Post not found"}
    end
  end

  def destroy
    @post = Post.find(params[:id])
    if @post
      @post.destroy
    else
      render json: {"message": "Post not found"}
    end
  end

  private

  def post_params
    {
      title: params[:title],
      content: params[:content],
      user_id: @current_user.id
    }
  end
end
