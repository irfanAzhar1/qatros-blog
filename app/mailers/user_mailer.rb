class UserMailer < ApplicationMailer
  default :from => "irfanqatros@gmail.com"

  def registration_confirmation()
    @user = params[:user]
    mail(:to => @user.email, :subject => "Registration Confirmation")
  end
end
