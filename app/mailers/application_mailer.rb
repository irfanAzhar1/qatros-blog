class ApplicationMailer < ActionMailer::Base
  default from: "irfanqatros@gmail.com"
  layout "mailer"
end
