require 'rails_helper'

RSpec.describe Post, type: :model do
  before(:each) do
    @user = User.new(
      name: "Irfan",
      email: "irfanazhar@mail.com",
      password: "qwerty123",
      )
    @user.save
    @post = Post.new(
      title: "post",
      content: "content",
      user_id: @user.id
    )
  end

  it "will save when title, content, and user_id are present" do
    post = @post
    expect(post.save).to eq(true)
  end

  it "will not save when title is not present" do
    post = @post
    post.title = nil
    expect(post.save).to eq(false)
  end

  it "will not save when content is not present" do
    post = @post
    post.content = nil
    expect(post.save).to eq(false)
  end

  it "will not save when user_id is not present" do
    post = @post
    post.user_id = nil
    expect(post.save).to eq(false)
  end

  it "will destroy all comment when post is destroyed" do
    post = @post
    post.save
    comment = Comment.new(
      body: "comment",
      user_id: @user.id,
      post_id: post.id
    )
    comment.save
    post.destroy
    expect(Comment.count).to eq(0)
  end

  it "will return a hash when new_attribute method is called" do
    post = @post
    post.save
    expect(post.new_attribute).to be_a(Hash)
  end

  it "will return a hash with comments when new attribute method is called" do
    post = @post
    post.save
    comment = Comment.new(
      body: "comment",
      user_id: @user.id,
      post_id: post.id
    )
    comment.save
    expect(post.new_attribute[:comments]).to be_a(Array)
  end
end
