require 'rails_helper'

RSpec.describe User, type: :model do
  before(:each) do
    @user = User.new(
      name: "Irfan",
      email: "irfan@mail.com",
      password: "qwerty123",
      dob: "2000-09-18",
      city: "Payakumbuh",
      bio: "mahasiswa biasa",
    )
  end

  it "will save when name, email, and password are present" do
    user = @user
    expect(user.save).to eq(true)
  end

  it "will not save when name is not present" do
    user = @user
    user.name = nil
    expect(user.save).to eq(false)
  end

  it "will not save when email is not present" do
    user = @user
    user.email = nil
    expect(user.save).to eq(false)
  end

  it "will not save when password is not present" do
    user = @user
    user.password = nil
    expect(user.save).to eq(false)
  end

  it "will not save when email is not unique" do
    user = @user
    user.save
    user2 = User.new(
      name: "Irfan",
      email: user.email,
      password: "qwerty123",
      )
  end

  it "will destroy all post and comment when user is destroyed" do
    user = @user
    user.save
    post = Post.new(
      title: "post",
      content: "content",
      user_id: user.id
    )
    post.save
    comment = Comment.new(
      body: "comment",
      user_id: user.id,
      post_id: post.id
    )
    comment.save
    user.destroy
    expect(Post.all.count).to eq(0)
    expect(Comment.all.count).to eq(0)
  end

  it "will not save when email is not valid" do
    user = @user
    user.email = "irfan"
    expect(user.save).to eq(false)
  end

  it "will return true when email is valid" do
    user = @user
    expect(user.email_valid?).to eq(true)
  end

  it "will return false when email is invalid" do
    user = @user
    user.email = "irfan"
    expect(user.email_valid?).to eq(false)
  end

  it "will return a hash when new_attribute method is called" do
    user = @user
    user.save
    expect(user.new_attribute).to be_a(Hash)
  end

  it "will save when email is activated" do
    user = @user
    user.save
    user.email_activate
    expect(user.email_confirmed).to eq(true)
    expect(user.confirm_token).to eq(nil)
  end
end
