require 'rails_helper'

RSpec.describe Comment, type: :model do
  before(:each) do
    @user = User.new(
      name: "Irfan",
      email: "irfanazhar@mail.com",
      password: "qwerty123",
      )
    @user.save

    @post = Post.new(
      title: "post",
      content: "content",
      user_id: @user.id
    )
    @post.save

    @comment = Comment.new(
      body: "comment",
      user_id: @user.id,
      post_id: @post.id
    )
  end

  it "will save when body, user_id, and post_id are present" do
    comment = @comment
    expect(comment.save).to eq(true)
  end

  it "will not save when body is not present" do
    comment = @comment
    comment.body = nil
    expect(comment.save).to eq(false)
  end

  it "will not save when post_id is not present" do
    comment = @comment
    comment.post_id = nil
    expect(comment.save).to eq(false)
  end

  it "will not save when user_id is not present" do
    comment = @comment
    comment.user_id = nil
    expect(comment.save).to eq(false)
  end

  it "will return a hash with user name when new_attribute method called" do
    comment = @comment
    comment.save
    output = comment.new_attribute
    expect(output).to be_a(Hash)
    expect(output[:user][:name]).to eq(@user.name)
  end
end
